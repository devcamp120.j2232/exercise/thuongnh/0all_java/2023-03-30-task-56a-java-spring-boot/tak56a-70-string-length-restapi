package com.devcamp.s50.stringlangthapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StringLangthApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(StringLangthApiApplication.class, args);
	}

}
